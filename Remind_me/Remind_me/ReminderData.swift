//
//  RemiderData.swift
//  Remind_me
//
//  Created by student on 24.01.2023..
//

import SwiftUI
import Foundation
import Combine

class ReminderData: ObservableObject{
    
    let remindersKey: String = "remiders_key"
    @Published var reminders: [Reminder] = []{
            didSet{
                SaveReminder()
            }
        }
    
    init(){
            GetReminder()
        }

    func SaveReminder(){
            if let encodedData = try? JSONEncoder().encode(reminders){
                UserDefaults.standard.set(encodedData, forKey: remindersKey)
                UserDefaults.standard.synchronize()
            }
        }
    
    func GetReminder(){
           guard let data = UserDefaults.standard.data(forKey: remindersKey),
                 let savedReminders = try? JSONDecoder().decode([Reminder].self, from: data)
           else {
               return
           }
           self.reminders = savedReminders
       }


    @MainActor @Sendable
    func fetchReminders() async {
        do {
            let url = URL(string: "https://remind-me-b6949-default-rtdb.europe-west1.firebasedatabase.app/reminders.json")!
            let (data, _) = try await URLSession.shared.data(from: url)
            
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            let json = try decoder.decode([String: Reminder].self, from: data)
            reminders = [Reminder](json.values)
        } catch let error {
            print(error)
        }
    }
    
    @MainActor @Sendable
    func sendReminders(reminder: Reminder) async {
        do {
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .iso8601
            let json = try encoder.encode(reminder)
            let url = URL(string: "https://remind-me-b6949-default-rtdb.europe-west1.firebasedatabase.app/reminders.json")!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = json
            
            let(_, response) = try await URLSession.shared.data(for: request)
            print(response)
        } catch let error {
            print(error)
        }
    }
    
    func delete(_ reminder: Reminder) {
        reminders.removeAll { $0.id == reminder.id }
    }
    
    func add(_ reminder: Reminder) {
        reminders.append(reminder)
        Task{
            await self.sendReminders(reminder: reminder)
        }
    }
    
    func exists(_ reminder: Reminder) -> Bool {
        reminders.contains(reminder)
    }
    
    func sortedReminders(period: Period) -> Binding<[Reminder]> {
        Binding<[Reminder]>(
            get: {
                self.reminders
                    .filter {
                        switch period {
                        case .nextSevenDays:
                            return $0.isNextWeek
                        case .nextThirtyDays:
                            return $0.isInAMonth
                        case .tomorrow:
                            return $0.isTomorrow
                        case .past:
                            return $0.isPast
                        case .future:
                            return $0.isInFuture
                        }
                    }
                    .sorted { $0.date < $1.date }
            },
            set: { reminders in
                for reminder in reminders {
                    if let index = self.reminders.firstIndex(where: { $0.id == reminder.id }) {
                        self.reminders[index] = reminder
                    }
                }
            }
        )
    }
}

extension Date {
    static func roundedHoursFromNow(_ hours: Double) -> Date {
        let exactDate = Date(timeIntervalSinceNow: hours)
        guard let hourRange = Calendar.current.dateInterval(of: .hour, for: exactDate) else {
            return exactDate
        }
        return hourRange.end
    }
}


enum Period: String, CaseIterable, Identifiable {
    case tomorrow = "Tomorrow"
    case nextSevenDays = "Next 7 Days"
    case nextThirtyDays = "Next 30 Days"
    case future = "In the future"
    case past = "Past"
    
    var id: String { self.rawValue }
    var name: String { self.rawValue }
}
