//
//  Remind_meApp.swift
//  Remind_me
//
//  Created by student on 24.01.2023..
//

import SwiftUI

@main
struct Remind_meApp: App {
    @StateObject var reminderData: ReminderData = ReminderData()
    
    var body: some Scene {
        WindowGroup {
            NavigationView{
                ContentView()
            }
            .environmentObject(reminderData)
            .task(reminderData.fetchReminders)
        }
    }
}
