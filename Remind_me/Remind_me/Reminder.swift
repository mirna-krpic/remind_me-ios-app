//
//  Reminder.swift
//  Remind_me
//
//  Created by student on 24.01.2023..
//

import SwiftUI

struct Reminder: Identifiable, Hashable, Codable{
    var id: String = UUID().uuidString
    var title: String
    var tasks: [SmallTask] 
    var date: Date = Date()
    var notificationTime: String = "None"
    
    var isComplete: Bool {
        tasks.allSatisfy{ $0.isCompleted }
    }
    
    var isPast: Bool{
        date < Date.now
    }
    
    var isTomorrow: Bool {
        !isPast && date <= Date.now.tommorow
    }
    
    var isNextWeek: Bool {
        !isPast && date <= Date.now.nextWeek && !isTomorrow
    }
    
    var isInAMonth: Bool {
        !isPast && date <= Date.now.nextMonth && !isNextWeek && !isTomorrow
    }
    
    var isInFuture: Bool {
        !isPast && date >= Date.now.inTheFuture && !isNextWeek && !isTomorrow
    }
    
    var remainingTaskCount: Int {
        tasks.filter { !$0.isCompleted }.count
    }
    
    static var example = Reminder(
        title: "Napravi mobilnu aplikaciju",
        tasks: [SmallTask(text: "Napisi kod"),
                SmallTask(text: "Pronadi potrebne materijale"),
                SmallTask(text: "Predaj zavrsenu aplikaciju")
               ],
        date: Date(timeIntervalSinceNow: 60 * 60 * 24 * 365 * 1.5)
    )
}

extension Date {
    var tommorow: Date {
        Calendar.autoupdatingCurrent.date(byAdding: .day, value: 1, to: self) ?? self
        //?? - u slucaju nil vrijednosti pridodaje vrijednost navedenu nakon upitnika
    }
    var nextWeek: Date {
        Calendar.autoupdatingCurrent.date(byAdding: .day, value: 7, to: self) ?? self
    }
    var nextMonth: Date {
        Calendar.autoupdatingCurrent.date(byAdding: .month, value: 1, to: self) ?? self
    }
    var inTheFuture: Date {
        Calendar.autoupdatingCurrent.date(byAdding: DateComponents(month: 1, day: 1), to: self) ?? self
    }
}
