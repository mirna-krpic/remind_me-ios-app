//
//  TaskView.swift
//  Remind_me
//
//  Created by student on 24.01.2023..
//

import SwiftUI

struct TaskView: View {
    
    @Binding var task : SmallTask
    var isEditing: Bool
    
    var body: some View {
        HStack{
            Button {
                task.isCompleted.toggle()
            } label: {
                Image(systemName: task.isCompleted ? "checkmark.circle.fill" : "circle")
            }
            .buttonStyle(.plain)
            Text(task.text)
        }
    }
}

struct TaskView_Previews: PreviewProvider {
    static var previews: some View {
        TaskView(task: .constant(SmallTask(text: "Do something!")), isEditing: false)
    }
}
