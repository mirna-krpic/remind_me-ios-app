//
//  RemidersListView.swift
//  Remind_me
//
//  Created by student on 24.01.2023..
//

import SwiftUI

struct RemindersListView: View {
    
    @EnvironmentObject var reminderData: ReminderData
    
    var body: some View {
        NavigationStack{
            List{
                ForEach(Period.allCases) {period in
                    if !reminderData.sortedReminders(period: period).isEmpty{
                        Section(content: {
                            ForEach(reminderData.sortedReminders(period: period)){
                                $reminder in
                                if !reminder.isComplete{
                                    NavigationLink {
                                        ReminderDetail(reminder: $reminder, isEditing: false)
                                    } label: {
                                        ReminderView(reminder: $reminder)
                                    }
                                    .swipeActions {
                                        Button(role: .destructive) {
                                            reminderData.delete(reminder)
                                        } label: {
                                            Label("Delete", systemImage: "trash")
                                        }
                                    }
                                }
                            }
                        }, header: {
                            Text(period.name)
                                .font(.callout)
                                .foregroundColor(.secondary)
                                .fontWeight(.bold)
                        })
                        
                    }
                }
                
            }.navigationTitle("My reminders")
            .toolbar{
                NavigationLink{
                    AddNewRemider()
                } label: {
                    Image(systemName: "plus")
                }
            }
        }
    }
}

struct RemidersListView_Previews: PreviewProvider {
    static var previews: some View {
        RemindersListView()
            .environmentObject(ReminderData())
    }
}
