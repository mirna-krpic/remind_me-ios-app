//
//  AddNewRemider.swift
//  Remind_me
//
//  Created by student on 27.01.2023..
//

import SwiftUI

struct AddNewRemider: View {
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var reminderData: ReminderData

    @State var title: String = ""
    @State var tasks: [SmallTask] = []
    @State var taskText: String = ""
    @State var date: Date = Date()
    @State private var showAlert = false
    @State var selectedTime: String = "None"
    let timeIntervals = ["None", "Day before", "Week before", "Month before" ,"Minute"]
    let notify = NotificationHandler()
    
    var body: some View {
        List{
            HStack{
                Image(systemName: "bell.fill")
                    .foregroundColor(.blue)
                    .padding(.horizontal, 5)
                
                TextField("New Reminder", text: $title)
                    .fontWeight(.regular)
            }
            DatePicker("Date", selection: $date)
                .labelsHidden()
                .listRowSeparator(.hidden)
                .datePickerStyle(.graphical)
            Text("Notification")
                .fontWeight(.bold)
            VStack{
                Picker("Time", selection: $selectedTime) {
                    ForEach(timeIntervals, id: \.self) {
                        Text($0)
                    }
                }
                .pickerStyle(.menu)
            }
            
            Text("Tasks")
                .fontWeight(.bold)
            
            ForEach($tasks) { $item in
                TaskView(task: $item, isEditing: false)
            }
            
            HStack{
                Button {
                    if !taskText.isEmpty {
                        tasks.append(SmallTask(text: taskText))
                    }
                    taskText = ""
                } label: {
                    Image(systemName: "plus")
                }
                .buttonStyle(.borderless)
                TextField("Add Task", text: $taskText)
            }
            
            HStack{
                Spacer()
                Button(action: {
                    if !title.isEmpty {
                        reminderData.add(
                            Reminder(title: title,
                                     tasks: tasks,
                                     date: date,
                                     notificationTime: selectedTime))
                        presentationMode.wrappedValue.dismiss()
                        notify.sendNotification(selectedDate: date, timeInterval: selectedTime)
                    } else {
                        showAlert = true
                    }
                }){
                    Text("Save")
                }
                .buttonStyle(.borderedProminent)
                .padding()
                .alert(isPresented: $showAlert) {
                    Alert(title: Text("Warning"),
                          message: Text("Title can not be empty."),
                          dismissButton: .default(Text("OK")))
                }
                Spacer()
            }
            
        }.navigationTitle("Add new reminder")
    }
}


struct AddNewRemider_Previews: PreviewProvider {
    static var previews: some View {
        AddNewRemider()
            .environmentObject(ReminderData())
    }
}
