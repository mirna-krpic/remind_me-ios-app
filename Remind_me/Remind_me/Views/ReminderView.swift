//
//  ReminderView.swift
//  Remind_me
//
//  Created by student on 24.01.2023..
//

import SwiftUI

struct ReminderView: View {
    
    @Binding var reminder: Reminder
    @EnvironmentObject var reminderData: ReminderData
    
    var symbolColor: Color {
        if reminder.isTomorrow{
            return .green
        }
        else if reminder.isNextWeek{
            return .mint
        }
        else if reminder.isInAMonth{
            return .cyan
        }
        else if reminder.isPast{
            return .indigo
        }
        else if reminder.isInFuture{
            return .purple
        }
        else {
            return .red
        }
    }
    
    var body: some View {
        HStack{
            Image(systemName: "bell.fill")
                .foregroundColor(symbolColor)
            VStack(alignment: .leading, spacing: 5){
                Text(reminder.title)
                    .fontWeight(.bold)
                Text("Remaining tasks: \(String( reminder.remainingTaskCount))")
                    .font(.caption2)
                Text(reminder.date.formatted(date: .long, time: .shortened))
                    .font(.caption2)
                    .foregroundStyle(.secondary)
            }
            .padding(.trailing, 50)
            Spacer()
            if reminder.isComplete{
                Image(systemName: "star.fill")
                    .foregroundColor(.yellow)
                    .foregroundStyle(.secondary)
            }
        }
    }
}

struct ReminderView_Previews: PreviewProvider {
    static var previews: some View {
        ReminderView(reminder: Binding.constant(Reminder.example))
    }
}
