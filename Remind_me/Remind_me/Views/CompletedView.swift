//
//  CompletedView.swift
//  Remind_me
//
//  Created by student on 03.02.2023..
//

import SwiftUI

struct CompletedView: View {
    
    @EnvironmentObject var reminderData: ReminderData
    
    var body: some View {
        NavigationStack{
            List{
                ForEach(Period.allCases) {period in
                    if !reminderData.sortedReminders(period: period).isEmpty{
                        ForEach(reminderData.sortedReminders(period: period)){
                            $reminder in
                            if reminder.isComplete{
                                NavigationLink {
                                    ReminderDetail(reminder: $reminder, isEditing: false)
                                } label: {
                                    ReminderView(reminder: $reminder)
                                }
                                .swipeActions {
                                    Button(role: .destructive) {
                                        reminderData.delete(reminder)
                                    } label: {
                                        Label("Delete", systemImage: "trash")
                                    }
                                }
                            }
                        }
                    }
                }
            }.navigationTitle("Complete tasks")
        }
    }
}

struct CompletedView_Previews: PreviewProvider {
    static var previews: some View {
        CompletedView().environmentObject(ReminderData())
    }
}
