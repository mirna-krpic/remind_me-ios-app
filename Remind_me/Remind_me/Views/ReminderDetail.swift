//
//  RemiderDetail.swift
//  Remind_me
//
//  Created by student on 25.01.2023..
//

import SwiftUI

struct ReminderDetail: View {
    
    @Binding var reminder: Reminder
    @State var taskText: String = ""
    @State var isEditing: Bool = false
    @EnvironmentObject var reminderData: ReminderData
    let notify = NotificationHandler()
    let timeIntervals = ["Day before", "Week before", "Month before" ,"Minute"]
    
    var symbolColor: Color {
        if reminder.isTomorrow{
            return .green
        }
        else if reminder.isNextWeek{
            return .mint
        }
        else if reminder.isInAMonth{
            return .cyan
        }
        else if reminder.isPast{
            return .indigo
        }
        else if reminder.isInFuture{
            return .purple
        }
        else {
            return .red
        }
    }
    
    var timeLeft: String {
        if reminder.isTomorrow{
            return "Reminder set for tommorow."
        }
        else if reminder.isNextWeek{
            return "Reminder set within a week."
        }
        else if reminder.isInAMonth{
            return "Reminder set within a month."
        }
        else if reminder.isPast{
            return "Reminder set for past time."
        }
        else if reminder.isInFuture{
            return "Reminder set for the future time"
        }
        else {
            return "Reminder"
        }
    }

    
    var body: some View {
        List{
            Text(timeLeft).fontWeight(.bold)
            HStack{
                Image(systemName: "bell.fill")
                    .foregroundColor(symbolColor)
             
                if isEditing {
                    TextField("New Reminder", text: $reminder.title)
                        .fontWeight(.regular)
                } else {
                    Text(reminder.title)
                        .font(.title2)
                        .fontWeight(.regular)
                }
                
            }
            if isEditing {
                DatePicker("Date", selection: $reminder.date)
                    .labelsHidden()
                    .listRowSeparator(.hidden)

            } else {
                HStack {
                    Text(reminder.date, style: .date)
                    Text(reminder.date, style: .time)
                }
                .listRowSeparator(.hidden)
            }
            
            if isEditing{
                Picker("Time", selection: $reminder.notificationTime) {
                    ForEach(timeIntervals, id: \.self) {
                        Text($0)
                    }
                }
                .pickerStyle(.menu)
                
                
            } else {
                Text("Notification")
                    .fontWeight(.bold)
                Text("Notification set ") +
                            Text(reminder.notificationTime.lowercased())
                                .fontWeight(.bold)
            }
            
            Text("Tasks")
                .fontWeight(.bold)
            
            ForEach($reminder.tasks) { $item in
                TaskView(task: $item, isEditing: isEditing)
            }
            .onDelete(perform: { indexSet in
                reminder.tasks.remove(atOffsets: indexSet)
            })
            HStack{
                Button {
                    if !taskText.isEmpty {
                        reminder.tasks.append(SmallTask(text: taskText))
                    }
                    taskText = ""
                } label: {
                    Image(systemName: "plus")
                }
                .buttonStyle(.borderless)
                TextField("Add Task", text: $taskText)
            }
            
            
        }.navigationTitle("Reminder detail")
        .toolbar{
            Button(action: {isEditing.toggle()})
            {
                Text("Edit")
            }
        }
    }
}

struct RemiderDetail_Previews: PreviewProvider {
    static var previews: some View {
        ReminderDetail(reminder: .constant(Reminder.example), isEditing: false)
    }
}
