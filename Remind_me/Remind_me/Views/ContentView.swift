//
//  ContentView.swift
//  Remind_me
//
//  Created by student on 24.01.2023..
//

import SwiftUI

struct ContentView: View {
    let notify = NotificationHandler()
    var body: some View {
        NavigationStack{
            Image("icon")
                .resizable()
                .frame(width: 100, height: 100)
                .clipShape(Circle())
            NavigationLink{
                RemindersListView()
            } label: {
                Label("My remiders", systemImage: "bell").labelStyle(.titleAndIcon)
            }.buttonStyle(.borderedProminent)
            NavigationLink{
                AddNewRemider()
            } label: {
                Label("Add new reminder", systemImage: "plus.circle").labelStyle(.titleAndIcon)
            }.buttonStyle(.borderedProminent)
            NavigationLink{
                CompletedView()
            } label: {
                Label("Complete tasks", systemImage: "star").labelStyle(.titleAndIcon)
            }.buttonStyle(.borderedProminent)

            Button("Request notification permission") {
                notify.askPermission()
            }
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
