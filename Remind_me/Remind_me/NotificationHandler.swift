//
//  NotificationHandler.swift
//  Remind_me
//
//  Created by student on 07.06.2023..
//

import Foundation
import UserNotifications

class NotificationHandler {
    func askPermission() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
            if success {
                print("Access granted!")
            } else if let error = error {
                print(error.localizedDescription)
            }
        }
    }

    func sendNotification(selectedDate: Date, timeInterval: String) {
        var trigger: UNNotificationTrigger?
        var triggerDate = Date.now
        
        if timeInterval != "None" {
            let content = UNMutableNotificationContent()
            content.title = "Remind Me App"
            content.sound = UNNotificationSound.default
                
            if timeInterval == "Day before" {
                triggerDate = Calendar.current.date(byAdding: .day, value: -1, to: selectedDate) ?? Date.now
                content.body = "Your reminder is set for tomorrow!"
                
            } else if timeInterval == "Week before" {
                triggerDate = Calendar.current.date(byAdding: .day, value: -7, to: selectedDate) ?? Date.now
                content.body = "Your reminder is set a week from now!"
                
            } else if timeInterval == "Month before" {
                triggerDate = Calendar.current.date(byAdding: .month, value: -1, to: selectedDate) ?? Date.now
                content.body = "Your reminder is set a month from now!"
                
            } else if timeInterval == "Minute" {
                triggerDate = Calendar.current.date(byAdding: .minute, value: -1, to: selectedDate) ?? Date.now
                content.body = "Your reminder is set for a minute!"
            }
            
            let dateComponents = Calendar.current.dateComponents([.day, .month, .year, .hour, .minute], from: triggerDate)
            
            trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
                
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request)
        }
    }
}
