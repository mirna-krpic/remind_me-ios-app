//
//  Task.swift
//  Remind_me
//
//  Created by student on 24.01.2023..
//

import SwiftUI
import Foundation

struct SmallTask: Identifiable, Hashable, Codable{
    var id = UUID()
    var text: String
    var isCompleted = false
}
